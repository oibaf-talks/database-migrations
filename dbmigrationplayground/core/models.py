# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Meetup(models.Model):
    # id (django adiciona o pseudo ID)
    date = models.DateField(auto_now_add=True, blank=False)
    name = models.CharField(null=True, default=None, max_length=10)
    # name = models.CharField(null=False, default='não informado', max_length=10)
