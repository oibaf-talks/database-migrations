# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Meetup

@admin.register(Meetup)
class MeetupAdmin(admin.ModelAdmin):
    pass
