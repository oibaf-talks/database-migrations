<?php


use Phinx\Db\Table\Column;
use Phinx\Migration\AbstractMigration;

class CriarTabelaTipo extends AbstractMigration
{
	/**
	 * Change Method.
	 *
	 * Write your reversible migrations using this method.
	 *
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 *
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    addCustomColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 *
	 * Any other destructive changes will result in an error when trying to
	 * rollback the migration.
	 *
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change() {
		/*
		 CREATE TABLE tipo
		(
		  id_tipo   int          not null primary key,
		  descricao varchar(255) not null
		);

		insert into tipo(id_tipo, descricao)
		values (1, 'primeiro tipo'),
			   (2, 'segundo tipo');

		 */
		$table = $this->table('tipo', ['id' => false, 'primary_key' => 'id_tipo']);

		$table->addColumn('id_tipo', 'integer')
			->addColumn((new Column())
				->setNull(false)
				->setName('descricao')
				->setType('string')
				->setLimit(255))
			->create();
	}
}
