<?php


use Phinx\Migration\AbstractMigration;

class PopularTabelaTipo extends AbstractMigration
{

	public function up() {
		$table = $this->table('tipo');
		$table->insert([
			[
				'id_tipo' => 1,
				'descricao' => 'primeiro tipo'
			], [
				'id_tipo' => 2,
				'descricao' => 'segundo tipo'
			],
		]);
		$table->saveData();
	}

	public function down() {
		$table = $this->table('tipo');
		$table->truncate();

	}
}
