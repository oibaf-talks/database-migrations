CREATE TABLE tipo
(
  id_tipo   int          not null primary key,
  descricao varchar(255) not null
);

insert into tipo(id_tipo, descricao)
values (1, 'primeiro tipo'),
       (2, 'segundo tipo');