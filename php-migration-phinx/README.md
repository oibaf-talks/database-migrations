# Requisitos
* Php >= 7.2 com as seguintes extens�es ativas:
    * pdo_pgsql
    * pdo_mysql
* Composer >=1.8

# Prepara��o...

1. Execute `composer install`
2. Copie o arquivo `.env-sample` para `.env`
3. Ajuste as portas conforme necess�rio em `.env`
4. Execute `docker-compose up` para criar os 3 bancos: 
   *  mysql j� populado
	* mysql sem dados 
	* postgres sem dados 
5. Aguarde at� que os bancos estejam operacionais.
6. Execute os testes com `composer run test`

# Walkthrough
Os testes devem ter sidos executados com sucesso; alguns, por�m, devem ter falhado:
1. Banco mysql_old n�o possui tabela de migrations
2. Bancos novos n�o possuem nem dados nem a tabela de migrations

## Resolvendo:

### Banco antigo sem tabela de migrations
Tente executar `composer run migrate` (� um comando de atalho criado para rodar migration nas 3 bases).
O seguinte erro aparecer�: 
```
Script php vendor/robmorgan/phinx/bin/phinx handling the phinx event returned with error code 1
Script @phinx migrate -e db_mysql_old was called via migrate-mysql-old
Script @migrate-mysql-old was called via migrate
```

Isso acontece porque, como o banco habia sido criado antes de existirem migrations, n�o existe a tabela "phinxlog", que registra quais migra��es j� foram aplicadas.

Para isso servem as _fake migrations_. Execute `vendor\bin\phinx migrate -e db_mysql_old --fake`

O seguinte resultado deve aparecer:
```
using seed paths
using environment db_mysql_old
using adapter mysql
using database my_schema
warning performing fake migrations

 == 20190319170323 CriarTabelaTipo: migrating
 == 20190319170323 CriarTabelaTipo: migrated 0.0073s

 == 20190319182250 PopularTabelaTipo: migrating
 == 20190319182250 PopularTabelaTipo: migrated 0.0077s

All Done. Took 0.0863s

```
 
Rode os testes; o primeiro agora deve ter passado.

### Bancos novos vazios

Execute novamente `composer run migrate`; os testes agora dever�o passar. 
