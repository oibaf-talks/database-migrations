<?php

use Codeception\Example;

class VerificaEstadoBancoCest
{

	/**
	 * @dataProvider getBancos
	 * @param IntegrationTester $I
	 * @param Example $e
	 */
	public function verificoTabelaTipo(IntegrationTester $I, Example $e) {
		$I->amConnectedToDatabase($e['key']);
		$I->seeNumRecords(2, 'tipo');
		$I->seeInDatabase('tipo', ['id_tipo' => 1, 'descricao' => 'primeiro tipo']);
		$I->seeInDatabase('tipo', ['id_tipo' => 2, 'descricao' => 'segundo tipo']);
	}

	private function getBancos() {
		return [
			['key' => 'db-mysql-old'],
			['key' => 'db-mysql-new'],
			['key' => 'db-postgres-new']
		];
	}


	/**
	 * @dataProvider getBancos
	 * @param IntegrationTester $I
	 * @param Example $e
	 */
	public function verificoExistenciaTabelaMigrations(IntegrationTester $I, Example $e) {
		$I->amConnectedToDatabase($e['key']);
		$I->seeInDatabase('phinxlog');
	}
}
