<?php

require_once('load_env.php');

return
	[
		'paths' => [
			'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
			'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
		],
		'environments' => [
			'default_migration_table' => 'phinxlog',

			'db_mysql_old' => [
				'adapter' => 'mysql',
				'host' => getenv('DB_HOST'),
				'name' => getenv('DB_NAME'),
				'user' => getenv('DB_ROOT_USER'),
				'pass' => getenv('DB_ROOT_PW'),
				'port' => getenv('DB_MYSQL_OLD_PORT'),
				'charset' => 'utf8',
			],
			'db_mysql_new' => [
				'adapter' => 'mysql',
				'host' => getenv('DB_HOST'),
				'name' => getenv('DB_NAME'),
				'user' => getenv('DB_ROOT_USER'),
				'pass' => getenv('DB_ROOT_PW'),
				'port' => getenv('DB_MYSQL_NEW_PORT'),
				'charset' => 'utf8',
			],
			'db_postgres_new' => [
				'adapter' => 'pgsql',
				'host' => getenv('DB_HOST'),
				'name' => getenv('DB_NAME'),
				'user' => getenv('DB_ROOT_USER'),
				'pass' => getenv('DB_ROOT_PW'),
				'port' => getenv('DB_POSTGRES_NEW_PORT'),
				'charset' => 'utf8',
			]
		],
		'version_order' => 'creation'
	];